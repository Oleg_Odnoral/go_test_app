package handlers

import (
	"encoding/json"
	"net/http"
)

var (
	books []Book
	data  BooksResponseWrapper
)

//GetBooks - return all available books list
func GetBooks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(wrapBooksData(books))
}

//GetBook - return book by its ID
func GetBook(w http.ResponseWriter, r *http.Request) {

}

//CreateBook - create new book
func CreateBook(w http.ResponseWriter, r *http.Request) {

}

//UpdateBook - update exist book by ID
func UpdateBook(w http.ResponseWriter, r *http.Request) {

}

//DeleteBook - delete book by ID
func DeleteBook(w http.ResponseWriter, r *http.Request) {

}

//GetMockData - Return slice with mock data
func GetMockData() []Book {
	return books
}

//AddMockData - Add mock data to slice
func AddMockData() {
	books = append(books, Book{
		ID:     "1",
		Isbn:   "322",
		Title:  "Test",
		Author: &Author{Name: "Oleg", Age: 25}})

	books = append(books, Book{
		ID:     "2",
		Isbn:   "1488",
		Title:  "LOL",
		Author: &Author{Name: "Oleg", Age: 25}})
}

func wrapBooksData(data []Book) BooksResponseWrapper {
	return BooksResponseWrapper{Data: data}
}
