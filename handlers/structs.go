package handlers

//BooksResponseWrapper wrap slice
type BooksResponseWrapper struct {
	Data []Book `json:"data"`
}

//Book describe book struct
type Book struct {
	ID     string  `json:"id"`
	Isbn   string  `json:"isbn"`
	Title  string  `json:"title"`
	Author *Author `json:"author"`
}

//Author describe author struct
type Author struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}
