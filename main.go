package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/User/rest/handlers"
	"github.com/gorilla/mux"
)

func main() {
	fmt.Println("Start func main")

	handlers.AddMockData()

	if len(handlers.GetMockData()) < 1 {
		panic("Empty mock data")
	} else {
		fmt.Println("All OK. Mock data exist")
	}

	r := mux.NewRouter()

	r.HandleFunc("/api/books", handlers.GetBooks).Methods("GET")
	r.HandleFunc("/api/books/{id}", handlers.GetBook).Methods("GET")
	r.HandleFunc("/api/books", handlers.CreateBook).Methods("POST")
	r.HandleFunc("/api/books/{id}", handlers.UpdateBook).Methods("PUT")
	r.HandleFunc("/api/books/{id}", handlers.DeleteBook).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8000", r))
}
